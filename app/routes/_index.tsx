import type { MetaFunction } from "@remix-run/node";

export const meta: MetaFunction = () => {
  return [
    { title: "Remix PWA Tutorial" },
    { name: "description", content: "Remix App!" },
  ];
};

export default function Index() {
  return (
    <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.8" }}>
      <h1>Remix PWA Tutorial</h1>
      <p>This is a Remix progressive web app</p>
    </div>
  );
}
